
########################################################
## build and run irods-4-stage (with icat database )  ##
########################################################

up&running in three steps: config, bulid and run.


1) CONFIG
put the rigth values into installer.sh file (db icat username and password are hardcoded )

2) BUILD
./setup.sh

>>>
reply on interactive iRODS installation:
(default value could be ok)

attention on
icat-section-------------------------------------------
user/passwd of icat db : same of installer

irods-section------------------------------------------
adminuser:rods password:choosePAsswd  

insert keys
"zone_key": "XOXOXO_ZONE_SID"
"negotiation_key": "xxxx_32_byte_key_for_agent__conn",
"server_control_plane_key": "TEMPORARY__32byte_ctrl_plane_key"


3) START
./start.sh


## CHECK ##
launch:
docker exec -it --user irods irods-4-stage ils

if response like '/tempZone/home/rods' :
 it's ok, irods-4-stage is up and running.

else : 
 try with :
 docker exec -it --user irods irods-4-stage /var/lib/irods/irodsctl restart
 (sometime postgresql have some delay)


## DATA ##
it's availible a docker volume called 'area-stage' for data sharing
